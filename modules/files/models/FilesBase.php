<?php

namespace app\modules\files\models;

use Yii;

/**
 * This is the model class for table "{{%files}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $origin_name
 * @property string $mime
 * @property string $ext
 * @property string $alt
 * @property integer $size
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 */
class FilesBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'origin_name', 'mime', 'ext'], 'required'],
            [['size', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'origin_name', 'alt'], 'string', 'max' => 255],
            [['mime'], 'string', 'max' => 16],
            [['ext'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'origin_name' => 'Origin Name',
            'mime' => 'Mime',
            'ext' => 'Ext',
            'alt' => 'Alt',
            'size' => 'Size',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
}
