<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.09.2016
 * Time: 20:59
 */

namespace app\modules\manager\controllers;


use Yii;
use app\models\SendEmailForm;
use yii\filters\AccessControl;
use yii\web\Controller;

class SendMailController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    'allow' => true,
//                    'actions' => ['index'],
//                ],
//            ],
//        ];
//    }

    public function init()
    {
        $this->view->title = 'Reset password';
        $this->layout = 'auth';
        parent::init();
    }

    public function actionIndex()
    {
        $model = new SendEmailForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->sendEmail()) {
                    Yii::$app->getSession()->setFlash('warning', 'Check email');
                    return $this->goHome();
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Not reset password');
                }
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}