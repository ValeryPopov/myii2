<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.09.2016
 * Time: 19:53
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;

/**
 * Модель формы для ввода нового пароля, при сбросе старого
 *
 * Class ResetPasswordForm
 * @package app\models
 */
class ResetPasswordForm extends Model
{
    public $password;
    private $_user;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            [['password'], 'string', 'min' => 6, 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Password',
        ];
    }

    public function __construct($key, $config = [])
    {
        if (empty($key) || !is_string($key)) {
            throw new InvalidParamException('Active key is empty');
        }

        $this->_user = User::findByActivateKey($key);

        if (!$this->_user) {
            throw new InvalidParamException('Active key is broken');
        }

        parent::__construct($config);
    }

    public function resetPassword()
    {
        /* @var $user User*/
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->resetActivateKey();

        return $user->save();
    }
}