<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2016
 * Time: 18:04
 */

namespace app\modules\manager\controllers;

use app\models\BirthDay;
use app\modules\files\models\Files;
use Yii;
use app\modules\manager\controllers\base\BehaviorsController;

class BirthDayController extends BehaviorsController
{
    public function init()
    {
        $this->view->title = "Днюха!";
        parent::init();
    }

    public function actionIndex()
    {
        $model = new BirthDay();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

        }

        $char = Yii::$app->session->get('char');
        Yii::$app->session->remove('char');

        return $this->render('index', ['model' => $model ,'char' => $char]);
    }
}