<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16.09.2016
 * Time: 19:21
 */

namespace app\helpers;

use Imagine\Image\Color;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use yii\imagine\BaseImage;

class image extends BaseImage
{
    public static function thumbnail($filename, $width, $height, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND)
    {
        $img = static::getImagine()->open(\Yii::getAlias($filename));

        $sourceBox = $img->getSize();
        $thumbnailBox = static::getThumbnailBox($sourceBox, $width, $height);

        if (($sourceBox->getWidth() <= $thumbnailBox->getWidth() && $sourceBox->getHeight() <= $thumbnailBox->getHeight()) || (!$thumbnailBox->getWidth() && !$thumbnailBox->getHeight())) {
            return $img->copy();
        }

        $img = $img->thumbnail($thumbnailBox, $mode);

        // create empty image to preserve aspect ratio of thumbnail
        $thumb = static::getImagine()->create($thumbnailBox, new Color(static::$thumbnailBackgroundColor, static::$thumbnailBackgroundAlpha));

        // calculate points
        $startX = 0;
        $startY = 0;
        if ($sourceBox->getWidth() < $width) {
            $startX = ceil($width - $sourceBox->getWidth()) / 2;
        } elseif ($sourceBox->getWidth() > $width && $img->getSize()->getWidth() < $width) {
            $startX = ceil($width - $img->getSize()->getWidth()) / 2;
        }
        if ($sourceBox->getHeight() < $height) {
            $startY = ceil($height - $sourceBox->getHeight()) / 2;
        } elseif ($sourceBox->getHeight() > $height && $img->getSize()->getHeight() < $height) {
            $startY = ceil($height - $img->getSize()->getHeight()) / 2;
        }

        $thumb->paste($img, new Point($startX, $startY));

        return $thumb;
    }
}