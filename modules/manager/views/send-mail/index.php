<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendEmailForm */
/* @var $form ActiveForm */
?>
<div class="login-box">
    <div class="login-logo">
        <a href="/manager/main"><b>Admin</b>LTE</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Enter you email</p>

        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'email')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Email',
            ])->label(false) ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="row">

            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Submit', [
                    'type' => 'submit',
                    'class' => 'btn btn-primary btn-block btn-flat',
                ]) ?>
            </div>
            <!-- /.col -->

        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

