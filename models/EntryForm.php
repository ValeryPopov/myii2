<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.07.2016
 * Time: 11:01
 */

namespace app\models;

use yii\base\Model;

class EntryForm extends Model
{
    public $name;
    public $email;

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],
        ];
    }
}