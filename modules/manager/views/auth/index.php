<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<!--<h3>Authorize</h3>-->
<!---->
<?php
//    $form = \yii\bootstrap\ActiveForm::begin([
//        'id' => 'login-form',
//    ]);
//
//    echo $form->field($model, 'username')->textInput();
//    echo $form->field($model, 'password')->passwordInput();
//
//    echo \yii\helpers\Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']);
//
//    \yii\bootstrap\ActiveForm::end();
//?>
<div class="login-box">
    <div class="login-logo">
        <a href="/manager/main"><b>Admin</b>LTE</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php $form = ActiveForm::begin() ?>
        <div class="form-group has-feedback">
            <?php if ($model->scenario === 'authThroughEmail'):?>
                <?= $form->field($model, 'email')->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'Email',
                ])->label(false) ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <?php else: ?>
                <?= $form->field($model, 'username')->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'Login',
                ])->label(false) ?>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            <?php endif;?>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Password',
                'type' => 'password',
            ])->label(false) ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <?= Html::activeCheckbox($model, 'rememberMe', [
                            'labelOptions' => [
                                'style' => 'padding-left:0px;'
                            ],
                        ]) ?>
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Sign In', [
                    'type' => 'submit',
                    'class' => 'btn btn-primary btn-block btn-flat',
                ]) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end() ?>

        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in
                using
                Google+</a>
        </div>
        <!-- /.social-auth-links -->

        <?= Html::a('I forgot my password', ['/manager/send-mail'])?><br>
        <a href="/manager/registration" class="text-center">Register a new membership</a>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
