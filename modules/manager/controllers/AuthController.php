<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.09.2016
 * Time: 17:51
 */

namespace app\modules\manager\controllers;

use app\models\LoginForm;
use app\modules\manager\controllers\base\BehaviorsController;
use yii;

class AuthController extends BehaviorsController
{
    public function init()
    {
        $this->layout = "auth";
        parent::init();
    }

    public function actionIndex()
    {
        $this->view->title = "Authorize";

        if (!Yii::$app->user->isGuest) {
            $this->goBack();
        }

        $authThroughEmail = Yii::$app->params['authThroughEmail'];

        $model = $authThroughEmail ? new LoginForm(['scenario' => 'authThroughEmail']) : new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect("/manager/main");
        }

        return $this->render("index", ['model' => $model]);
    }
}