<?php
return [
    '/' => 'site/index',
    '/about' => 'site/about',
    '/contact' => 'site/contact',
    '/entry' => 'site/entry',
    '/country' => 'country/index',
    '/login' => 'site/login',
    '' => '',
    '' => '',
];