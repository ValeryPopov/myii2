<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.09.2016
 * Time: 19:23
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Модель формы отправки письма для сброса пароля
 *
 * Class SendEmailForm
 * @package app\models
 */
class SendEmailForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            [['email'], 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'filter' => [
                    'status' => User::STATUS_ACTIVE
                ],
                'message' => 'Email not found',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }

    /**
     * Отправка письма для сброса пароля
     *
     * @return bool
     */
    public function sendEmail()
    {
        $model = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($model) {
            $model->setActivateKey();
            if ($model->save()) {
                return Yii::$app->mailer->compose('resetPassword', ['user' => $model])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . " (created automate)"])
                    ->setTo($this->email)
                    ->setSubject('Reset password to ' . Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}