<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2016
 * Time: 16:22
 */

namespace app\modules\manager\controllers;


use app\models\AccountActivation;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class ActivateAccountController extends Controller
{
    public function init()
    {
        $this->view->title = "Активация акаунта";
        parent::init();
    }

    public function actionIndex($key)
    {
        try {
            $user = new AccountActivation($key);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($user->activateAccount()) {
            Yii::$app->session->setFlash('success', 'Активация прошла успешно ' . Html::encode($user->username));
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка активации');
            Yii::error('Ошибка при активации');
        }

        return $this->redirect(Url::to(['/manager/main']));
    }
}