<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\widgets\Breadcrumbs;

?>
<?php $this->beginContent("@app/modules/admin/views/layouts/base.php"); ?>
<div class="wrap">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])?>
        <?= $content?>
    </div>
</div>
<footer>
    <div align="center">&copy; <?= date('Y')?></div>
</footer>
<?php $this->endContent(); ?>
