<?php
use yii\helpers\Html;

$this->title = 'Entry-confirm';
$this->params['breadcrumbs'][] = $this->title;
?>

<p>Вы ввели следующую информацию:</p>

<ul>
    <li><label>Имя</label>: <?php echo Html::encode($model->name); ?></li>
    <li><label>E-mail</label>: <?php echo Html::encode($model->email); ?></li>
</ul>
