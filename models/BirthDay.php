<?php

namespace app\models;

use Imagine\Image\ManipulatorInterface;
use Yii;
use app\modules\files\models\Files;
use \yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%birth_day}}".
 *
 * @property integer $id
 * @property integer $photo_id
 */
class BirthDay extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%birth_day}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo_id' => 'Фотография',
        ];
    }

    /**
     * @param $fileUploaded
     * @param $field
     * @return bool
     * @throws \Exception
     */
    public function saveFile($fileUploaded, $field){
        $file = new Files();
        $file->file = $fileUploaded;

        if(!$file->saveUploadedFile()){
            $this->addError($field, 'Saving file fail.');
            return false;
        }

        $this->$field = $file->id;

//        if(isset($this->oldAttributes[$field])){
//            $oldFile = Files::findOne(['id' => $this->oldAttributes[$field]]);
//            if($oldFile)
//                $oldFile->safeDelete();
//        }

        return true;
    }

    public function beforeSave($insert){
        if(parent::beforeSave($insert)){

            // сохраняем photo
            if(!$fileUploaded = UploadedFile::getInstance($this, 'photo_id') OR !$this->saveFile($fileUploaded, 'photo_id'))
                $this->photo_id = ($this->oldAttributes AND $this->oldAttributes['photo_id']) ? $this->oldAttributes['photo_id'] : null;


            return true;
        }
        else
            return false;
    }

    public function getPhotoImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'photo_id']);
    }

    public function getPhotoBootstrapFileInputOptions(){
        $photo = $this->photo_id ? $this->getPhotoImage()->one() : null;

        $initialPreview = [];
        $initialPreviewConfig = [];
        if(isset($photo->id) AND !empty($photo->id)) {
            $initialPreview[] = Html::img($photo->getThumbnailUrl(100,100,ManipulatorInterface::THUMBNAIL_INSET), [
                'class'=>'file-preview-image',
                'alt'=>$photo->alt,
                'id' => $photo->id,
            ]);
            $initialPreviewConfig[] = [
                'key' => $photo->id,
                'url' => Files::getDeleteUrl(),
                'extra' => ['id' => $photo->id],
            ];
        }

        return [
            'deleteUrl' => Files::getDeleteUrl(),
            'showRemove' => false,
            'showUpload' => false,
            'layoutTemplates' => [
                'preview'=> '<div class="file-preview {class}">
                                <div class="{dropClass}">
                                    <div class="file-preview-thumbnails"></div>
                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error"></div>
                                </div>
                            </div>',
            ],
            'browseLabel' =>  'Загрузить картинку',
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,];
    }
}
