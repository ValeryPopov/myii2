<?php

use yii\db\Migration;

/**
 * Handles the creation for table `news_table`.
 */
class m160703_160525_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_table', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'content' => $this->text(),
        ]);

        $this->insert('news_table', ['title' => 'News One', 'content' => 'Hello World']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_table');
    }
}
