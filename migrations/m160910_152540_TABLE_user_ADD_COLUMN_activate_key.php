<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m160910_152540_TABLE_user_ADD_COLUMN_activate_key extends Migration
{
    public $tableName = '{{%user}}';

    public function up()
    {
        try {
            $this->addColumn($this->tableName, 'activate_key', Schema::TYPE_STRING . " AFTER auth_key");
            return true;
        } catch (Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            $this->down();
            return false;
        }
    }

    public function down()
    {
        try {
            $tableToCheck = Yii::$app->db->schema->getTableSchema($this->tableName);

            if ($tableToCheck->getColumn('activate_key')) {
                $this->dropColumn($this->tableName ,'activate_key');
                return true;
            }

            return false;
        } catch (Exception $e) {
            echo "Exception: ", $e->getMessage(), "\n";
            return false;
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
