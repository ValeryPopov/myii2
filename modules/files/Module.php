<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16.09.2016
 * Time: 19:30
 */

namespace app\modules\files;


use yii\base\Model;

class Module extends Model
{
    public $controllerNamespace = 'modules\files\controllers';

    public $domain = '';
    public $rootPath = '';
    public $mainDir = '';

    public function init()
    {
        parent::init();

        \Yii::configure($this, require(__DIR__ . '/config.php'));
        // custom initialization code goes here
    }
}