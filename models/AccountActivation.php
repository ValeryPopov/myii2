<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2016
 * Time: 15:44
 */

namespace app\models;

use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/* @property string $username */

class AccountActivation extends Model
{
    /* @var $user User*/
    private $_user;

    /**
     * По ключу активации ищет пользователя
     *
     * AccountActivation constructor.
     * @param array $key
     * @param array $config
     */
    public function __construct($key, $config = [])
    {
        if (empty($key) || !is_string($key)) {
            throw new InvalidParamException('Key is empty!');
        }

        $this->_user = User::findByActivateKey($key);

        if (!$this->_user) {
            throw new InvalidParamException('Key is not valid');
        }

        parent::__construct($config);
    }

    /**
     * @return bool
     */
    public function activateAccount()
    {
        $user = $this->_user;
        $user->status = User::STATUS_ACTIVE;
        $user->resetActivateKey();

        return $user->save();
    }

    /**
     * Возвращает логин активированного пользователя
     *
     * @return string
     */
    public function getUserName()
    {
        $user = $this->_user;
        return $user->username;
    }
}