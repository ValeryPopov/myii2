<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Handles the creation for table `table_files`.
 */
class m160916_145334_create_table_files extends Migration
{
    public $tableName = '{{%files}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $tableOptions = null;

            if (Yii::$app->db->driverName === 'mysql') {
               $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            }

            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'origin_name' => $this->string(255)->notNull(),
                'mime' => $this->string(16)->notNull(),
                'ext' => $this->string(8)->notNull(),
                'alt' => $this->string(255),
                'size' => $this->integer(11),
                'created_at' => Schema::TYPE_DATETIME,
                'updated_at' => Schema::TYPE_DATETIME,
                'user_id' => $this->integer(11)->defaultValue(null),
            ], $tableOptions);

            return true;
        } catch (Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            $this->down();

            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        try {
            $tableToCheck = Yii::$app->db->schema->getTableSchema($this->tableName);

            if (is_object($tableToCheck)) {
                $this->dropTable($this->tableName);
            }

            return true;
        } catch (Exception $e) {
            echo 'Exception while down ', $e->getMessage(), "\n";

            return false;
        }
    }
}
