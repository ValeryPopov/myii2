<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.09.2016
 * Time: 22:31
 */

namespace app\models;


use Yii;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $status;

    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password'], 'required'],
            [['username'], 'string', 'min' => 3, 'max' => 255],
            [['password'], 'string', 'min' => 6, 'max' => 255],
            [
                ['username'], 'unique',
                'targetClass' => User::className(),
                'message' => 'Это имя уже занято',
            ],
            [['email'], 'email'],
            [
                ['email'], 'unique',
                'targetClass' => User::className(),
                'message' => 'Такой почтовый ящик уже зарегистрирован',
            ],
            [
                ['status'], 'default', 'value' => User::STATUS_ACTIVE,
                'on' => 'default',
            ],
            [
                ['status'], 'default', 'value' => User::STATUS_NOT_ACTIVE,
                'on' => 'activateThroughEmail',
            ],
            [['status'], 'in', 'range' => [
                User::STATUS_NOT_ACTIVE,
                User::STATUS_ACTIVE,
            ]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Эл. почта',
            'password' => 'Пароль',
        ];
    }

    public function registration()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($this->scenario === 'activateThroughEmail') {
            $user->setActivateKey();
        }

        return $user->save() ? $user : null;
    }

    public function sendActivationEmail($user)
    {
        return Yii::$app->mailer->compose('activationEmail', ['user' => $user])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . '(send automatic)'])
            ->setTo($this->email)
            ->setSubject('Activate you account to ' . Yii::$app->name)
            ->send();
    }
}