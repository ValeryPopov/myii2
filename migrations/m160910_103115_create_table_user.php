<?php

use yii\db\Migration;
use \yii\db\mysql\Schema;

/**
 * Handles the creation for table `table_user`.
 */
class m160910_103115_create_table_user extends Migration
{
    public $tableName = '{{%user}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $tableOptions = null;

            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            }

            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(),
                'username' => $this->string()->notNull()->unique(),
                'email' => $this->string()->notNull()->unique(),
                'password_hash' => $this->string(),
                'status' => $this->integer(4)->defaultValue(10),
                'auth_key' => $this->string(),
                'created_at' => Schema::TYPE_DATETIME, //. $this->dateTime()->notNull()->defaultValue('0000-00-00 00:00:00'),
                'updated_at' => Schema::TYPE_DATETIME, //. $this->dateTime()->notNull()->defaultValue('0000-00-00 00:00:00'),
            ], $tableOptions);

            return true;
        } catch (Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            $this->down();

            return false;
        }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        try {
            $tableToCheck = Yii::$app->db->schema->getTableSchema($this->tableName);

            if (is_object($tableToCheck)) {
                $this->dropTable($this->tableName);
            }
        } catch (Exception $e) {
            echo 'Exception while down ', $e->getMessage(), "\n";
        }
    }
}
