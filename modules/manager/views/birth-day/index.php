<?php
/* @var $char $_GET['char']*/
/* @var $model BirthDay */

use app\models\BirthDay;
use dosamigos\fileinput\BootstrapFileInput;
use yii\bootstrap\Modal;
use \yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<h3>Поздравляю с днем варенья<?= $char ? ' ' . $char : ''?>!</h3>
<?php

    echo \yii\helpers\Html::a(
        'Модальное окно',
        ['#'],
        [
            'data-toggle' => 'modal',
            'data-target' => '#search',
            'class' => 'btn btn-warning',
        ]
    ) . "<br><br>";

    Modal::begin([
//        'size' => 'modal-sm', //modal-lg
        'options' => [ //вынос кнопки модального окна за приделы виджета
            'id' => 'search',
        ],
        'header' => '<h2>Модальное окно</h2>',
        'toggleButton' => [
            'label' => 'Модальное окно',
            'tag' => 'button',
            'class' => 'btn btn-danger',
        ],
        'footer' => 'Подвла окна',
    ]);

    echo "Hello beauty full!";

    Modal::end();

    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
    echo $form->field($model, 'photo_id', [
        'options' => [
            'class' => 'col-xs-12',
        ],
    ])->widget(BootstrapFileInput::className(), [
        'options' => [
            'multiple' => false,
            'accept' => 'image/*',
            'id' => 'teachers-form-photo',
        ],
        'clientOptions' => $model->getPhotoBootstrapFileInputOptions(),
    ]);?>

    <div class="form-group col-xs-12">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end();
?>