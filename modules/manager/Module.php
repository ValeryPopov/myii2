<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.09.2016
 * Time: 15:12
 */

namespace app\modules\manager;


class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\manager\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}