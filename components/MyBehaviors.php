<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2016
 * Time: 19:34
 */

namespace app\components;


use yii\base\Behavior;
use yii\base\Controller;

class MyBehaviors extends Behavior
{
    private $_controller;
    private $_action;
    private $_replaceChar;

    public function setController($value)
    {
        $this->_controller = $value;
    }

    public function getController()
    {
        return $this->_controller;
    }

    public function setAction($value)
    {
        $this->_action = $value;
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function setReplaceChar($value)
    {
        $this->_replaceChar = ucwords($value);
    }

    public function getReplaceChar()
    {
        return $this->_replaceChar;
    }

    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    public function beforeAction()
    {
        if ($this->controller == 'birth-day' && $this->action === 'index') {
            \Yii::$app->session->set('char', $this->replaceChar);
        }
    }
}