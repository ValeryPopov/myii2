<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.09.2016
 * Time: 21:16
 */

namespace app\modules\manager\controllers;


use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

class ResetPasswordController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    'allow' => true,
//                    'actions' => ['index'],
//                ],
//            ],
//        ];
//    }

    public function init()
    {
        $this->view->title = 'Change password';
        $this->layout = "auth";
        parent::init();
    }

    public function actionIndex($key)
    {
        try {
            $model = new ResetPasswordForm($key);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                Yii::$app->getSession()->setFlash('warning', 'Password is changed');

                return $this->redirect(['/manager/main']);
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}