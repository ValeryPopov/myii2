<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.09.2016
 * Time: 17:42
 */

namespace app\modules\admin\controllers\base;


use yii\web\Controller;
use Yii;

class BaseController extends Controller
{
    public $layout = "main";

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect("/admin/auth");
        }

        return parent::beforeAction($action);
    }
}