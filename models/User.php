<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use \yii\db\Expression;

/**
 * Class User
 * @package app\models
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property integer $status
 * @property string $activate_key
 */

class User extends ActiveRecord  implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 10;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function rules()
    {
        return [
            [['username', 'email', 'password_hash'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'status'], 'required'],
            [['email'], 'email'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'default', 'value' => 10],
            [['auth_key'], 'string', 'max'=> 255],
            [['username'], 'string', 'min' => 3, 'max' => 255],
            [['password_hash'], 'required', 'on' => 'create'],
            [['username'], 'unique', 'message' => 'Это имя уже занято'],
            [['email'], 'unique', 'message' => 'Такой почтовый ящик уже зарегистрирован'],
            [['activate_key'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Login',
            'email' => 'Email',
            'password_hash' => 'Password',
            'status' => 'Status',
            'auth_key' => 'Auth key',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     *
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by email
     *
     * @param $email
     * @return null|static
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Generate activate key
     */
    public function setActivateKey()
    {
        $this->activate_key = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Find user by activate key
     *
     * @param $key
     * @return null|static
     */
    public static function findByActivateKey($key)
    {
        if (!static::isActivateKeyExpire($key)) {
            return null;
        }

        return static::findOne(['activate_key' => $key]);
    }

    /**
     * Checks to exist activate key
     *
     * @param $key
     * @return bool
     */
    public static function isActivateKeyExpire($key)
    {
        if (empty($key)) {
            return false;
        }

        $expire = Yii::$app->params['activateKeyExpire'];
        $parts = explode('_', $key);
        $timeStamp = (int)end($parts);

        return time() <= $timeStamp + $expire;
    }

    /**
     * Remove activate key
     */
    public function resetActivateKey()
    {
        $this->activate_key = null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey ? $this->auth_key : null;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
}
