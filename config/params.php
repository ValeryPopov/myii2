<?php

return [
    'adminEmail' => 'admin@example.com',
    'authThroughEmail' => true,             //авторизация через email
    'activateKeyExpire' => 60 * 60,         //время действия ключа 1 час
    'activateThroughEmail' => true,         //активация через emeil
];
