<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;

/* @var $model \app\models\LoginForm */
?>

<h3>Authorize</h3>
<div class="container">
<!--    --><?php
//        echo Html::beginForm(['default/index'], 'post');
//
//        echo Html::label("login") . "<br/>";
//        echo Html::input("text", "user", Yii::$app->request->post('user')) . "<br/>";
//
//        echo "<br/>" . Html::submitInput("Вход") . "<br/>";
//        echo Html::endForm();
//    ?>
    <?php
        $form = ActiveForm::begin([
            'id' => 'login-form',
            'fieldConfig' => [
                'template' => "{label}{input}{error}",
            ],
        ]);

        echo $form->field($model, 'username')->textInput(['autoFocus' => true]);
        echo $form->field($model, 'password')->passwordInput();
        echo $form->field($model, 'rememberMe')->checkbox([
            'template' => "{input}{label}{error}",
        ]);

        echo Html::submitButton('Вход', ['name' => 'login-button']);

        ActiveForm::end();
    ?>
</div>