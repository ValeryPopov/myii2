<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.09.2016
 * Time: 11:55
 */

namespace app\modules\manager\controllers;

use app\models\User;
use app\modules\manager\controllers\base\BehaviorsController;
use Yii;
use app\models\RegistrationForm;
use yii\helpers\Html;

/**
 * Регистрация пользователя
 *
 * Class RegistrationController
 * @package app\modules\manager\controllers
 */
class RegistrationController extends BehaviorsController
{
    public function init()
    {
        $this->layout = "auth";
        $this->view->title = 'Регистрация';
        parent::init();
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $emailActivation = Yii::$app->params['activateThroughEmail'];

        $model = $emailActivation ? new RegistrationForm(['scenario' => 'activateThroughEmail']) : new RegistrationForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->registration()) {
                if ($user->status === User::STATUS_ACTIVE) {
                    if (Yii::$app->getUser()->login($user)) {
                        return $this->redirect(['/manager/main']);
                    }
                } elseif ($user->status === User::STATUS_NOT_ACTIVE) {
                    if ($model->sendActivationEmail($user)) {
                        Yii::$app->session->setFlash('success', 'Письмо отправлено на email ' . Html::encode($user->email));
                    } else {
                        Yii::$app->session->setFlash('error', 'Ошибка, письмо не отправлено');
                        Yii::error('Ошибка отправки письма');
                    }
                    return $this->refresh();
                }
            } else {
                Yii::$app->session->setFlash('error', 'Возникла ошибка при регистрации');
                Yii::error('Ошибка при регистрации');
                return $this->refresh();
            }
        }

        return $this->render('index', ['model' => $model]);
    }
}