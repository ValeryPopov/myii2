<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model app\models\EntryForm*/

$this->title = 'Entry';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'name')->label('Ваше имя'); ?>
    <?php echo $form->field($model, 'email')->label('Ваш E-mail'); ?>
    <div class="form-group">
        <?php echo Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
    </div>
<?php ActiveForm::end(); ?>

