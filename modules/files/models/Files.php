<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16.09.2016
 * Time: 19:09
 */

namespace app\modules\files\models;

use app\helpers\image;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;

class Files extends FilesBase
{
    public function init(){
        parent::init();
    }

    public $file, $url, $fileUrl;

    public $mainDir;
    public $domain;
    public $rootPath;

    public static $imageMime = ['image/gif', 'image/jpeg', 'image/png'];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
//        $module = \Yii::$app->getModule('modules/files');
        $this->domain = 'http://myii2/files';
        $this->mainDir = 'files';
        $this->rootPath = '../';
    }

    public static function getDeleteUrl(){
        return (isset(\Yii::$app->components['request']['baseUrl']) ? \Yii::$app->components['request']['baseUrl'] : '') . '/files/default/delete';
    }

    public static function getSortUrl(){
        return (isset(\Yii::$app->components['request']['baseUrl']) ? \Yii::$app->components['request']['baseUrl'] : '') . '/files/default/sort';
    }

    /*
     * возвращает полную ссылку на файл
     */
    public function getFileUrl(){
        return $this->domain . '/' . $this->getDir() . '/' . $this->getFullName();
    }


    /*
     * $name имя файла
     * возвращает полную ссылку на файл $name
     */
    public function getFileUrlByName($name){
        return  $this->domain . '/' . $this->getDir() . '/' . $name;
    }

    /*
     * возвращает директория файла на основе даты создания (<year>/<month>)
     */
    public function getDir(){
        $dir = explode('-', $this->created_at);
        return "{$dir[0]}/$dir[1]";
    }

    /*
     * возвращает полное имя файла(<name>.<ext>)
     */
    public function getFullName(){
        return "{$this->name}.{$this->ext}";
    }

    /*
     * меняет размер изображения
     */
    public function getResizeImage($width = 0, $height = 0){

        $dir = $this->getDirRealPath();
        $origin = "{$dir}/{$this->getFullName()}";


        $imagine = new \yii\imagine\Image();
        $tools = $imagine->getImagine();
        $open = $tools->open($origin);
        $size = $open->getSize();
        $oWidth = $size->getWidth();
        $oHeight = $size->getHeight();

        $fileName = ($oWidth + $width) . "x" . ($oHeight + $height) . ".{$this->ext}";

        if(!file_exists("{$dir}/{$this->name}_resize"))
            mkdir("{$dir}/{$this->name}_resize");

        if(!file_exists("{$dir}/{$this->name}_resize/$fileName")){
            $open->resize(new Box($oWidth + $width, $oHeight + $height));
            $open->save("{$dir}/{$this->name}_resize/{$fileName}");
        }

        return $this->getFileUrlByName("/{$this->name}_resize/{$fileName}");
    }

    /*
     * создает thumbnail для изображения
     */
    public function getThumbnailUrl($width, $height, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND){
        $dir = $this->getDirRealPath();
        $thumbName = "{$width}x{$height}.{$this->ext}";

        if(!file_exists("{$dir}/{$this->name}_thumbs"))
            mkdir("{$dir}/{$this->name}_thumbs");

        if(!file_exists("{$dir}/{$this->name}_thumbs/$thumbName")){
            // 15.06.16 старый  thumbname от \yii\imagine\BaseImage почему-то перестал центрировать картинку, а прижимал её к краю бокса. Пришлось написать хэлпер-наследник
            image::thumbnail("{$dir}/{$this->getFullName()}", $width, $height, $mode)
                ->save("{$dir}/{$this->name}_thumbs/{$thumbName}", ['quality' => 75]);

            /* \yii\imagine\Image::thumbnail("{$dir}/{$this->getFullName()}", $width, $height, $mode)
                 ->save("{$dir}/{$this->name}_thumbs/{$thumbName}", ['quality' => 75]);*/
        }

        return $this->getFileUrlByName("{$this->name}_thumbs/{$thumbName}");

    }

    /*
     * возвращает физический путь до файла
     */
    public function getFileRealPath(){
        return "{$this->getDirRealPath()}/{$this->getFullName()}";
    }

    /*
     * возвращает физический путь папки файла
     */
    public function getDirRealPath(){
        $structure = "{$this->mainDir}/{$this->getDir()}";
        $basePath = realpath(\Yii::getAlias($this->rootPath));
        return "$basePath/$structure";
    }

    /*
     * return false if not save file or not save model
     */
    public function saveUploadedFile($files_list_id = null)
    {
        // если не передали файл то выходим
        if(!$this->file){
            return false;
        }

        $year = date('Y', time());
        $month = date('m', time());
        $this->url = "{$year}/{$month}";
        $structure = "{$this->mainDir}/{$this->url}";
        $basePath = realpath(\Yii::getAlias($this->rootPath));

        $absolutePath = "$basePath/$structure";

        if (!file_exists($absolutePath)) {
            mkdir($absolutePath, 0777, true);
        }

//        $this->id = uuid::binUuid();
        $this->name = md5($this->id);
        $this->ext = $this->file->extension;
        $this->origin_name = $this->file->baseName ? : $this->file->name ? : 'UNDEFINED NAME';
        $this->mime = $this->file->type;
        $this->size = $this->file->size;
//        $this->files_list_id = null;
        $this->user_id = \Yii::$app->user->id;

//        if($files_list_id)
//            $this->files_list_id = $files_list_id;
        if(!$this->file->saveAs("{$absolutePath}/{$this->getFullName()}"))
        {
            return false;
        }

        return $this->save();
    }

    public function afterDelete()
    {
        if (parent::beforeDelete()) {
            $fileDir = $this->getDirRealPath();
            $file = "{$fileDir}/{$this->getFullName()}";
            // удаляем файл
            if(file_exists($file))
                unlink($file);
            // удаляем миниатюры
            if (file_exists("{$fileDir}/{$this->name}_thumbs/")) {
                foreach (glob("{$fileDir}/{$this->name}_thumbs/*") as $file)
                    unlink($file);
                rmdir("{$fileDir}/{$this->name}_thumbs/");
            }

            // удаляем ресайзы
            if (file_exists("{$fileDir}/{$this->name}_resize/")) {
                foreach (glob("{$fileDir}/{$this->name}_resize/*") as $file)
                    unlink($file);
                rmdir("{$fileDir}/{$this->name}_resize/");
            }

            return true;
        } else {
            return false;
        }
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $date = new \DateTime();
            $now = $date->format('Y-m-d H:i:s');

            if($this->isNewRecord) {
                $this->created_at = $now;
            }

            $this->updated_at = $now;
            return true;
        } else {
            return false;
        }
    }

    public function isImage()
    {
        $isImage =in_array($this->mime, self::$imageMime);
        if(!$isImage)
            $this->addError('file', 'This file is not image.');
        return $isImage;
    }
}