<?php

use app\modules\manager\assets\ManagerAsset;
use yii\helpers\Html;

ManagerAsset::register($this);
?>
<?php $this->beginPage()?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= Yii::$app->name . " | " . $this->title?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php $this->head()?>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
<?php $this->beginBody()?>
    <?= $content?>
<?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>
