<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.09.2016
 * Time: 19:19
 */

namespace app\modules\admin\controllers;

use app\models\LoginForm;
use yii\web\Controller;
use Yii;

class AuthController extends Controller
{
    public $layout = "main";

    public function actionIndex()
    {
        $this->view->title = "Authorize";
        $this->view->params['breadcrumbs'][] = "Authorize";

        if (!Yii::$app->user->isGuest) {
            return $this->goBack();
        }

        $model = new LoginForm();



        return $this->render("index", ['model' => $model]);
    }
}