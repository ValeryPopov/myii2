<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2016
 * Time: 17:18
 */

namespace app\modules\manager\controllers\base;

use app\components\MyBehaviors;
use yii\base\Exception;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class BehaviorsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'denyCallback' => function($rule, $action) {
//                    throw new Exception('Нет доступа!');
//                },
                'rules' => [
                    [ //доступ к регистрации только не авторизованным пользователям
                        'allow' => true,
                        'controllers' => ['manager/registration', 'manager/auth'],
                        'actions' => ['index'],
                        'verbs' => ['GET', 'POST'],
                        'roles' => ['?'],
                    ],
                    [ //разрешает actionSearch во всех контроллерах без дополнительных условий
                        'allow' => true,
                        'actions' => ['search'],
                    ],
                    [ //доступ к action при условии, что сегодня нужная дата
                        'allow' => true,
                        'controllers' => ['manager/birth-day'],
                        'actions' => ['index'],
                        'ips' => ['127.0.0.*'],
                        'matchCallback' => function($rule, $action) {
                            return date('d-m') == '17-09';
                        }
                    ],
                ],
            ],
            'replaceChar' => [
                'class' => MyBehaviors::className(),
                'controller' => Yii::$app->controller->id,
                'action' => Yii::$app->controller->action->id,
                'replaceChar' => Yii::$app->request->get('char'),
            ],
        ];
    }
}