<?php
/**
 * @var $user \app\models\User
 */

use yii\helpers\Html;

echo "Hello " . Html::encode($user->username);
echo Html::a('Activate you account for this link ', Yii::$app->urlManager->createAbsoluteUrl([
    '/manager/activate-account',
    'key' => $user->activate_key,
]));