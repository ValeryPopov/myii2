<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.09.2016
 * Time: 15:24
 */

namespace app\modules\manager\controllers;


use app\modules\manager\controllers\base\BaseController;
use Yii;

class MainController extends BaseController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = "Manager panel";

        $this->view->params['breadcrumbs'][] = "Панель управления";

        return $this->render("index");
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/manager/main']);
    }
}