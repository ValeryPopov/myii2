<?php
/**
 * @var $user \app\models\User
 */

use yii\helpers\Html;

echo 'Hello! ' . Html::encode($user->username);
echo Html::a('Changes password to link ',
    Yii::$app->urlManager->createAbsoluteUrl([
        '/manager/reset-password',
        'key' => $user->activate_key,
    ])
);