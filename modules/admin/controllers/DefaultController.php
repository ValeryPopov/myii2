<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\base\BaseController;
use Yii;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = "Admin panel";
        $this->view->params['breadcrumbs'][] = "admin";

        return $this->render('index');
    }
}
