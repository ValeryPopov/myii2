<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_birth_day`.
 */
class m160916_143213_create_table_birth_day extends Migration
{
    public $tableName = '{{%birth_day}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $tableOptions = null;

            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
            }

            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(),
                'photo_id' => $this->integer()->defaultValue(null),
            ], $tableOptions);

            return true;
        } catch (Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            $this->down();

            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        try {
            $tableToCheck = Yii::$app->db->schema->getTableSchema($this->tableName);

            if (is_object($tableToCheck)) {
                $this->dropTable($this->tableName);
            }
        } catch (Exception $e) {
            echo 'Exception while down ', $e->getMessage(), "\n";
        }
    }
}
